#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using JackieSoft.Blueprint.Conversion;
using UnityEngine;

#endregion

public class Test : MonoBehaviour
{
    public int testInt;
    public int[][] testArray = new int[3][];
    public int[,] testArray1 = new int[3, 3];
    public TestEnum testEnum;
    public List<TestEnum> testEnums;

    private void Start()
    {
        Log<TestClass>();
        Debug.Log("---------------------------");
        Log<TestMonoBehaviour>();
    }

    private static void Log<T>()
    {
        var type = typeof(T);
        var fieldInfos = type.GetFields(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public);
        foreach (var fieldInfo in fieldInfos)
        {
            Debug.Log(fieldInfo.Name + "|" + fieldInfo.ReflectedType);
        }
    }
}

public class TestClass
{
    public static int staticProperty = 6;
    public const int constProperty = 10;
    public int testProperty1;
    public int testProperty2;
    public TestEnum testProperty3;
}

public class TestMonoBehaviour : MonoBehaviour
{
    public int testProperty1;
    public int testProperty2;
    public TestEnum testProperty3;
}

public enum TestEnum
{
    None,
    FirstTest,
    SecondTest
}