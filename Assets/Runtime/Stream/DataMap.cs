using System.Collections.Generic;
using UnityEngine;

namespace JackieSoft.Blueprint.Stream
{
    public class DataMap
    {
        public DataMap(Vector2Int size, IReadOnlyList<string> columnName)
        {
            Size = size;
            data = new string[size.x, size.y];
            column = new Dictionary<string, int>(columnName.Count);
            for (var i = 0; i < columnName.Count; i++)
            {
                column.Add(columnName[i], i);
            }
        }
        
        private string[,] data;
        private Dictionary<string, int> column;
        public Vector2Int Size { get; }
        public List<string> ColumNames => new List<string>(column.Keys);
        public List<int> ColumId => new List<int>(column.Values);
        
        public string this[int row, string columName] {
            get
            {
                if (column.TryGetValue(columName, out var columIndex) && row < Size.y)
                {
                    return data[columIndex, row];
                }
                return string.Empty;
            }

            set
            {
                if (column.TryGetValue(columName, out var columIndex) && row < Size.y)
                {
                    data[columIndex, row] = value;
                }
            }
        }
    }
}