﻿namespace JackieSoft.Blueprint.Stream
{
    using System.Threading.Tasks;

    public interface IStream
    {
        Task<DataMap> Read();
        Task Write(DataMap map);
    }

    public class OnlineGoogleSheet : IStream
    {
        private IStream _streamImplementation;

        public Task<DataMap> Read()
        {
            throw new System.NotImplementedException();
        }

        public Task Write(DataMap map)
        {
            return _streamImplementation.Write(map);
        }

    }

    public class OfflineGoogleSheet : IStream
    {
        public Task<DataMap> Read()
        {
            throw new System.NotImplementedException();
        }

        public Task Write(DataMap map)
        {
            throw new System.NotImplementedException();
        }
    }
}