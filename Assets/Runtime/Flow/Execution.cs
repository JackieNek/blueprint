﻿using System;
using JackieSoft.Blueprint.Stream;
using UnityEngine;
using UnityEngine.Serialization;

namespace JackieSoft.Blueprint.Flow
{
    
    
    public interface IBinding { }
    public interface IBindingMode { }
    
    [Serializable]
    public class FolderBinding : IBinding
    {
        [SerializeField, FolderSelector] private string path;
        [SerializeReference, SubclassSelector] private IDestinationCombine destination = new IndexCombine();
        [SerializeReference, SubclassSelector] private IStream stream = new OfflineGoogleSheet();
        [SerializeReference, SubclassSelector] private IBindingMode mode = new ComponentBinding();
    }

    public interface IDestinationCombine
    {
        string MakePath(int index, string path, DataMap dataMap);
    }

    public class IndexCombine : IDestinationCombine
    {
        public string MakePath(int index, string path, DataMap dataMap)
        {
            throw new NotImplementedException();
        }
    }
    
        

    public class ComponentBinding : IBindingMode
    {
        
    }

    public class FolderSelectorAttribute : PropertyAttribute
    {
    }


}