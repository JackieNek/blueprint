﻿namespace JackieSoft.Blueprint.Conversion {
    using System.Globalization;

    [CustomConverter(typeof(double))]
    public sealed class DoubleConverter : Converter<double> {

        protected override double ConvertToObjectFrom(string data) {
            return double.TryParse(data, NumberStyles.Number, TypeConverterCache.Setting.Provider, out var value) ? value : 0;
        }

        protected override string ConvertToStringFrom(double value) {
            return value.ToString(TypeConverterCache.Setting.Provider);
        }
    }
}