﻿namespace JackieSoft.Blueprint.Conversion {
    [CustomConverter(typeof(bool))]
    public sealed class BooleanConverter : Converter<bool> {

        protected override bool ConvertToObjectFrom(string data) {
            return bool.TryParse(data, out var value) && value;
        }

        protected override string ConvertToStringFrom(bool value) {
            return value.ToString();
        }
    }
}