﻿namespace JackieSoft.Blueprint.Conversion {
    using System.Globalization;

    [CustomConverter(typeof(float))]
    public sealed class FloatConverter : Converter<float> {
        protected override float ConvertToObjectFrom(string data) {
            return float.TryParse(data, NumberStyles.Float, TypeConverterCache.Setting.Provider, out var value) ? value : 0;
        }

        protected override string ConvertToStringFrom(float value) {
            return value.ToString(TypeConverterCache.Setting.Provider);
        }
    }
}