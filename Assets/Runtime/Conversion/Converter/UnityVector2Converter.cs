﻿namespace JackieSoft.Blueprint.Conversion {
    using System.Globalization;
    using UnityEngine;
    
    [CustomConverter(typeof(Vector2))]
    public sealed class UnityVector2Converter : Converter<Vector2> {

        protected override Vector2 ConvertToObjectFrom(string data) {
            var axesData = data.Split('_');
            return float.TryParse(axesData[0], NumberStyles.Float, TypeConverterCache.Setting.Provider, out var x) &&
                   float.TryParse(axesData[1], NumberStyles.Float, TypeConverterCache.Setting.Provider, out var y) 
                ? new Vector2(x, y)
                : Vector2.zero;
        }

        protected override string ConvertToStringFrom(Vector2 value) {
            return $"{value.x.ToString(CultureInfo.CurrentCulture)}_{value.y.ToString(CultureInfo.CurrentCulture)}";
        }
    }
}