﻿namespace JackieSoft.Blueprint.Conversion {

    [CustomConverter(typeof(string))]
    public sealed class StringConverter : Converter<string> {
        protected override string ConvertToStringFrom(string value) {
            return value;
        }

        protected override string ConvertToObjectFrom(string data) {
            return data;
        }
    }

}