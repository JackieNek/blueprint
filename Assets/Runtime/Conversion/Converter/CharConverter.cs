﻿namespace JackieSoft.Blueprint.Conversion {
    [CustomConverter(typeof(char))]
    public sealed class CharConverter : Converter<char> {

        protected override char ConvertToObjectFrom(string data) {
            return char.TryParse(data, out var value) ? value : default;
        }

        protected override string ConvertToStringFrom(char value) {
            return value.ToString();
        }
    }
}