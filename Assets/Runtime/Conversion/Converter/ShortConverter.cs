﻿namespace JackieSoft.Blueprint.Conversion {
    using System.Globalization;

    [CustomConverter(typeof(short))]
    public sealed class ShortConverter : Converter<short> {
        protected override short ConvertToObjectFrom(string data) {
            return short.TryParse(data, NumberStyles.Integer, TypeConverterCache.Setting.Provider,out var value) ? value : (short)0;
        }

        protected override string ConvertToStringFrom(short value) {
            return value.ToString(TypeConverterCache.Setting.Provider);
        }
    }

}