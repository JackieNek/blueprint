﻿namespace JackieSoft.Blueprint.Conversion {
    using System.Globalization;

    [CustomConverter(typeof(decimal))]
    public class DecimalConverter : Converter<decimal> {

        protected override decimal ConvertToObjectFrom(string data) {
            return decimal.TryParse(data,NumberStyles.Number, TypeConverterCache.Setting.Provider, out var value) ? value : 0;
        }

        protected override string ConvertToStringFrom(decimal value) {
            return value.ToString(TypeConverterCache.Setting.Provider);
        }
    }
}