﻿namespace JackieSoft.Blueprint.Conversion {
    using System.Globalization;

    [CustomConverter(typeof(long))]
    public sealed class LongConverter : Converter<long> {
        protected override long ConvertToObjectFrom(string data) {
            return long.TryParse(data, NumberStyles.Integer, TypeConverterCache.Setting.Provider, out var value) ? value : 0;
        }

        protected override string ConvertToStringFrom(long value) {
            return value.ToString(TypeConverterCache.Setting.Provider);
        }
    }
}