﻿namespace JackieSoft.Blueprint.Conversion {
    using System.Globalization;

    [CustomConverter(typeof(int))]
    public sealed class IntegerConverter : Converter<int> {
        protected override int ConvertToObjectFrom(string data) {
            return int.TryParse(data, NumberStyles.Integer, TypeConverterCache.Setting.Provider, out var value) ? value : 0;
        }

        protected override string ConvertToStringFrom(int value) {
            return value.ToString(TypeConverterCache.Setting.Provider);
        }
    }


}