﻿namespace JackieSoft.Blueprint.Conversion {
    using System.Globalization;
    using UnityEngine;

    [CustomConverter(typeof(Vector3))]
    public sealed class UnityVector3Converter : Converter<Vector3> {

        protected override Vector3 ConvertToObjectFrom(string data) {
            var axesData = data.Split('_');
            return float.TryParse(axesData[0], NumberStyles.Float, TypeConverterCache.Setting.Provider, out var x) &&
                   float.TryParse(axesData[1], NumberStyles.Float, TypeConverterCache.Setting.Provider, out var y) &&
                   float.TryParse(axesData[2], NumberStyles.Float, TypeConverterCache.Setting.Provider, out var z)
                ? new Vector3(x, y, z)
                : Vector3.zero;
        }

        protected override string ConvertToStringFrom(Vector3 value) {
            return $"{value.x.ToString(CultureInfo.CurrentCulture)}_{value.y.ToString(CultureInfo.CurrentCulture)}_{value.z.ToString(CultureInfo.CurrentCulture)}";
        }
    }

}