﻿namespace JackieSoft.Blueprint.Conversion {
    [CustomConverter(typeof(byte))]
    public sealed class ByteConverter : Converter<byte> {

        protected override byte ConvertToObjectFrom(string data) {
            return byte.TryParse(data, out var value) ? value : default;
        }

        protected override string ConvertToStringFrom(byte value) {
            return value.ToString();
        }
    }
}