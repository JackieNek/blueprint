﻿namespace JackieSoft.Blueprint.Conversion {
    using System;

    public class MissingConverterException : Exception {
        public MissingConverterException(string message) : base(message) { }
    }
}