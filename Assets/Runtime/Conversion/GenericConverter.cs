﻿namespace JackieSoft.Blueprint.Conversion {
    using System;

    public abstract class GenericConverter : IGenericConverter {
        private Type _type;
        
        object IConverter.StringToObject(string data) {
            return ConvertToObjectFrom(data, _type);
        }

        string IConverter.ObjectToString(object value) {
            return value == null ? string.Empty : ConvertToStringFrom(value);
        }

        IGenericConverter IGenericConverter.With(Type type) {
            _type = type;
            return this;
        }

        protected abstract object ConvertToObjectFrom(string data, Type type);

        protected abstract string ConvertToStringFrom(object value);
    }
}