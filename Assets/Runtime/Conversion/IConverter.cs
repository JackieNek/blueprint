﻿namespace JackieSoft.Blueprint.Conversion {
    using System;

    public interface IConverter {
        
        object StringToObject(string data);

        string ObjectToString(object value);
    }

    public interface IGenericConverter : IConverter {

        IGenericConverter With(Type dataType);
    }

    public interface INormalConverter : IConverter {
        
    }
}