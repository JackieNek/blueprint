﻿namespace JackieSoft.Blueprint.Conversion {
    using System;

    public abstract class Converter<T> : INormalConverter {

        object IConverter.StringToObject(string data) {
            return ConvertToObjectFrom(data);
        }

        string IConverter.ObjectToString(object value) {
            if (!(value is T t)) return string.Empty;
            return ConvertToStringFrom(t);
        }

        protected abstract T ConvertToObjectFrom(string data);

        protected abstract string ConvertToStringFrom(T value);
        
    }
}