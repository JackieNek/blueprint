﻿namespace JackieSoft.Blueprint.Conversion {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;

    [CustomConverter(typeof(List<>))]
    public sealed class ListConverter : GenericConverter {
        
        protected override object ConvertToObjectFrom(string data, Type type) {
            var elementData = data.Split(TypeConverterCache.Setting.enumeratorDelimiter);
            var elementConverter = TypeConverterCache.GetConverter(type.GenericTypeArguments[0]);
            var list = (IList)Activator.CreateInstance(type);
            foreach (var elementDatum in elementData)
                list.Add(elementConverter.StringToObject(elementDatum));
            return list;
        }

        protected override string ConvertToStringFrom(object value) {
            var builder = new StringBuilder();
            var list = (IList)value;
            for (var i = 0; i < list.Count; i++) {
                var element = list[i];
                var elementConverter = TypeConverterCache.GetConverter(element.GetType());
                builder.Append(elementConverter.ObjectToString(element));
                if (i != list.Count - 1) builder.Append(TypeConverterCache.Setting.enumeratorDelimiter);
            }
            return builder.ToString();
        }
    }
}