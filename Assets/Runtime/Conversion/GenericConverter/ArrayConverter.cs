﻿namespace JackieSoft.Blueprint.Conversion {
    using System;
    using System.Text;

    public sealed class ArrayConverter : GenericConverter {
        
        protected override object ConvertToObjectFrom(string data, Type dataType) {
            var array = (Array)Activator.CreateInstance(dataType);
            var elementConverter = TypeConverterCache.GetConverter(dataType.GetElementType());
            var elementData = data.Split(TypeConverterCache.Setting.enumeratorDelimiter);
            for (var i = 0; i < elementData.Length; i++) {
                array.SetValue(elementConverter.StringToObject(elementData[i]), i);
            }
            return array;
        }

        protected override string ConvertToStringFrom(object value) {
            var builder = new StringBuilder();
            var array = (Array)value;
            
            for (var index = 0; index < array.Length; index++) {
                var arrayElement = array.GetValue(index);
                var elementConverter = TypeConverterCache.GetConverter(arrayElement.GetType());
                builder.Append(elementConverter.ObjectToString(arrayElement));
                if (index != array.Length - 1) builder.Append(TypeConverterCache.Setting.enumeratorDelimiter);
            }

            return builder.ToString();
        }
    }

}