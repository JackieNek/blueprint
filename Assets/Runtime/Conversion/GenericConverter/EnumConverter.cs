﻿namespace JackieSoft.Blueprint.Conversion {
    using System;

    public sealed class EnumConverter : GenericConverter {
        
        protected override object ConvertToObjectFrom(string data, Type type) {
            return Enum.Parse(type, data, false);
        }

        protected override string ConvertToStringFrom(object value) {
            return value.ToString();
        }
    }
}