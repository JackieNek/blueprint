﻿namespace JackieSoft.Blueprint.Conversion {
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;

    public static class TypeConverterCache {
        
        private readonly static Dictionary<Type, INormalConverter> MapNormalConverters = InitConverter<INormalConverter>();
        private readonly static Dictionary<Type, IGenericConverter> MapGenericConverters = InitConverter<IGenericConverter>();
        private readonly static IGenericConverter EnumConverter = new EnumConverter();
        private readonly static IGenericConverter ArrayConverter = new ArrayConverter();

        public static Settings Setting { get; set; } = new Settings();

        private static Dictionary<Type, T> InitConverter<T>() where T : IConverter {
            var iConverterType = typeof(T);
            var converterTypeEnumerable = iConverterType.Assembly.GetTypes()
                .Where(t => !t.IsInterface && !t.IsAbstract && iConverterType.IsAssignableFrom(t));

            var mapConverterCache = new Dictionary<Type, T>();

            foreach (var converterType in converterTypeEnumerable) {
                var converter = (T) Activator.CreateInstance(converterType);
                var customConverters = converterType.GetCustomAttributes<CustomConverterAttribute>();
                foreach (var customConverter in customConverters)
                {
                    mapConverterCache.Add(customConverter.Type, converter);
                }
            }

            return mapConverterCache;
        }

        public static IConverter GetConverter(Type type) {

            if (MapNormalConverters.TryGetValue(type, out var normalConverter))
                return normalConverter;

            if (MapGenericConverters.TryGetValue(type, out var genericConverter))
                return genericConverter.With(type);

            if (type.IsEnum)
                return EnumConverter.With(type);

            if (type.IsArray)
                return ArrayConverter.With(type);
            
            throw new MissingConverterException($"Missing converter for {type}");
        }
    }

    [Serializable]
    public class Settings {
        public char enumeratorDelimiter = ',';
        public char dictionaryPairDelimiter = ':';
        public string cultureFormat = "en-US";

        private IFormatProvider _provider;

        public IFormatProvider Provider {
            get => _provider ??= CultureInfo.CreateSpecificCulture(cultureFormat);
        }
    }
}