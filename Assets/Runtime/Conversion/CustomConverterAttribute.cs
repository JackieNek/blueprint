﻿namespace JackieSoft.Blueprint.Conversion {
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public sealed class CustomConverterAttribute : Attribute {
        public readonly Type Type;
        
        public CustomConverterAttribute(Type type) {
            Type = type;
        }
        
    }
    
}