using System;

namespace JackieSoft.Blueprint {
    
    [AttributeUsage(AttributeTargets.Field)]
    public class ColumnAttribute : Attribute
    {
        public string name;
    }
}