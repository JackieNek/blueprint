﻿using UnityEngine;

namespace JackieSoft.Blueprint.Editor
{
    using UnityEditor;

    public class EditorWindow : UnityEditor.EditorWindow
    {
        [MenuItem("Jackie Soft/Blueprint")]
        public static void ShowWindow() => GetWindow<EditorWindow>("Blueprint").Show();

        private SerializedObject _serializedObject;
        private Vector2 scrollVector2;
        
        private void OnEnable()
        {
            _serializedObject = new SerializedObject(Settings.instance);
        }

        private void OnDisable()
        {
            Settings.instance.Save();
        }

        public void OnGUI()
        {
            _serializedObject.Update();
            scrollVector2 = EditorGUILayout.BeginScrollView(scrollVector2);
            EditorGUILayout.PropertyField(_serializedObject.FindProperty("binding"), GUIContent.none);
            EditorGUILayout.EndScrollView();
        }
    }
}