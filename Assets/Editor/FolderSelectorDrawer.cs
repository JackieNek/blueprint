using JackieSoft.Blueprint.Flow;
using UnityEditor;
using UnityEngine;

namespace JackieSoft.Blueprint.Editor
{

    [CustomPropertyDrawer(typeof(FolderSelectorAttribute))]
    public class FolderSelectorDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.String)
            {
                EditorGUI.PropertyField(position, property, label);
            }
            else
            {
                var with = position.width;
                position.width = with - 40;
                EditorGUI.LabelField(position, label, new GUIContent(property.stringValue));
                position.x += (with - 30);
                position.width = 30;
                if (GUI.Button(position, new GUIContent("...")))
                {
                    var path = EditorUtility.OpenFolderPanel("Select Directory", "Assets", "");
                    property.stringValue = path.Substring(Application.dataPath.Length + 1);
                }
            }
        }
    }
    
    
}