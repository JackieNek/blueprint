﻿using JackieSoft.Blueprint.Flow;
using UnityEngine;

namespace JackieSoft.Blueprint.Editor {
    using UnityEditor;

    
    [FilePath("ProjectSettings/BlueprintSetting.asset", FilePathAttribute.Location.ProjectFolder)]
    public class Settings : ScriptableSingleton<Settings> {

        public Conversion.Settings settings;
        public void Save() => base.Save(true);
        
        [SerializeReference, SubclassSelector]
        private IBinding[] binding;
    }
}