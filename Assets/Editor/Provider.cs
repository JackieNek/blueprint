﻿namespace JackieSoft.Blueprint.Editor {
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine.UIElements;

    public class Provider : SettingsProvider {
        private Editor _editor;

        private Provider(string path, SettingsScope scopes, IEnumerable<string> keywords = null) : base(path, scopes, keywords) { }

        public override void OnActivate(string searchContext, VisualElement rootElement) {
            _editor = Editor.CreateEditor(Settings.instance);
        }

        public override void OnDeactivate() {
            _editor = null;
            Settings.instance.Save();
        }

        public override void OnGUI(string searchContext) {
            if (ActiveEditorTracker.HasCustomEditor(_editor)) _editor.OnInspectorGUI();
            else _editor.DrawDefaultInspector();
        }


        [SettingsProvider]
        public static SettingsProvider CreateBlueprintProvider() {
            return new Provider("Project/Blueprint", SettingsScope.Project);
        }
    }
}